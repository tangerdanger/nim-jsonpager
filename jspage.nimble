# Package

version       = "0.1.0"
author        = "Ryan Cotter"
description   = "A JSON pager"
license       = "MIT"
srcDir        = "src"
bin           = @["jspage"]



# Dependencies

requires "nim >= 1.0.4"
requires "simple_parseopt"

task make, "Build the app":
  exec "mkdir -p bin"
  exec "nim c --out:bin/jspage src/jspage.nim "


task ex, "Run the app":
  exec "mkdir -p bin"
  exec "nim c --out:bin/jspage src/jspage.nim"
  exec "bin/jspage -filename 'tests/pjson.json' -indentmax 3"
