## Super simple, pages json
import json, simple_parseopt, streams, os, strutils, strformat, sequtils

type 
  Index = ref object of RootObj
    enter, exit: int

const 
  NEW_INDEX = ['{', '[']
  NEW_INDENT = ['}', ']']
  NEW_LINE = [',']
  NL = '\n'
  MAX_INDENTS = 1000

let options = get_options:
  filename: string
  indentmax: int = 1

var 
  lineCounter: int = 0
  strm: StringStream
  indexMax: int = 0

proc openFileForStream(filename: string): StringStream =
  ## Convert a file stream to a stringstream
  assert existsFile filename
  newStringStream(openFileStream(filename, fmRead).readAll())

func newIndex(s, e: int): Index = 
  new result
  result.enter = s
  result.exit = e


proc `$`(i: Index): string =
  fmt"Index({i.enter} - {i.exit})"


iterator parseContent(maxIndent: int): JsonNode = 
  ## Given a loaded file, parse the contents and 
  ## split the results into up to `maxIndent` nested indentations
  var 
    buffer: char
    indent = 0
    breakpoints: array[0 .. MAX_INDENTS, Index]
    index: Index

  strm.read(buffer)
  while not strm.atEnd():
    let position = strm.getPosition()
    if buffer.isSpaceAscii:
      # Skip white space
      strm.read(buffer)
      continue

    if buffer in NEW_INDEX:
      # For opening braces, we increment the index
      if indent < maxIndent:
        index = newIndex(position, 0)
        breakpoints[indent] = index
      indent.inc
    elif buffer in NEW_INDENT:
      # For closing braces, we decrement the indent
      # and we update the index end if we're closing a tag
      # within our maxindent range
      if indent <= maxIndent:
        index = breakpoints[indent - 1]
        index.exit = position
        breakpoints[indent - 1] = index
        index = newIndex(0, 0)
      indent.dec

      # Break if we're at our final closing tag
      if indent == 0: break
    strm.read(buffer)

  # Don't process nil breakpoints
  for idx in breakpoints.filter(proc(x: Index): bool = not x.isNil):
    let 
      range = idx.enter .. idx.exit
      size = idx.exit - idx.enter + 1
    var jsBuffer: array[1000, char] 

    # Set the position of the stream to just before the chunk to extract
    strm.setPosition(idx.enter - 1)

    let numRead = strm.readData(addr(jsBuffer), size)

    # Make a string out of our buffer
    let fragment = join(jsBuffer, "")

    # Return our parsed json and strip any trailing \x00's
    yield parseJson(fragment.strip(trailing = true))

when isMainModule:
  strm = openFileForStream(options.filename)
  for i in parseContent(options.indentmax):
    echo pretty(
      i, 
      indent = options.indentmax
    )

  strm.close()
